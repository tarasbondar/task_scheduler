/* init */

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(256) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`), /*A_I???*/
  ADD KEY `updated_by` (`updated_by`) USING BTREE;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks.updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL; /*???*/

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'admin', 'admin@a.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');



/* 29.10.2019*/

ALTER TABLE `users` ADD `session` VARCHAR(40) NOT NULL AFTER `password`;
