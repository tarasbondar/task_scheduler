<?php

namespace Test;

use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase {

    protected $stack;

    protected function setUp() :void {
        $this->stack = [];
    }

    public function testGetAll() {
        //$stack = [];
        $this->assertEmpty($this->stack);
        //$stack = ['stack'];
        //return $stack;
    }

    /**
     * @dataProvider arrays
     */
    public function testPush(array $stack)
    {
        array_push($stack, 'foo');
        $this->assertSame(1, count($stack));
        $this->assertNotEmpty($stack);

        return $stack;
    }

    public function arrays() {
        return [
            'empty' => [ [] ],
            //'one_elem' => [ [1] ],
            //'two_elems' => [ ['1', '2'] ]
        ];
    }

    public function uselessTest() {
        $this->assertTrue(true, 'This should already work.');
        $this->markTestIncomplete('This test is useless.');
    }


}
