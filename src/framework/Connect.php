<?php

namespace Framework;

class Connect {

    private static $_instance = NULL;

    public static function instance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new Connect();
        }
        return self::$_instance;
    }

    protected function __construct() {}

    function __destruct(){}

    public function getConnection($dbname, $host, $username, $password) {
        $conn = null;
        try {
            $dsn = 'mysql:dbname=' . $dbname . ';host=' . $host;
            $conn = new \PDO($dsn, $username, $password);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $conn;

        } catch (PDOException $e) {
            throw $e;
        } catch(Exception $e) {
            throw $e;
        }
    }

    public function __clone() {
        return false;
    }
    public function __wakeup() {
        return false;
    }
}