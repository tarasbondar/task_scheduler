<?php

namespace Framework;

use Exception;

class Template {

    private string $viewPath = APP_ROOT . '/src/views';
    private string $baseView = 'base.php';
    private array $reservedVariables = ['application_name', 'body', 'log_button'];
    private array $directives = ['foreach'];

    public function __construct() {

    }

    public function getView($controller, array $variables = [], array $system = []) {
        if (!$this->validateVariables($variables)) {
            http_response_code(404);
            throw new Exception('Unacceptable view variable given.', 409);
        };
        //add directives filter

        $parts = explode('::', $controller);
        $directory = $this->getDirectory($parts[0]);
        $file = $this->getFile($parts[1]);

        $viewPath = $this->viewPath.'/'.$directory.'/'.$file.'.php';
        if (file_exists($viewPath)) {
            $baseView = file_get_contents($this->viewPath.'/'.$this->baseView);
            $body = file_get_contents($viewPath);
            $view = str_replace('{{ body }}', $body, $baseView);

            foreach ($this->directives as $directive) {
                //search for '@'.$directive
                //get code between each '@'.$directive and '@end'.$directive
                //process code, write into $view
            }

            foreach ($variables as $key => $value) {
                $view = str_replace('{{ '.$key.' }}', $value, $view);
            }

            if (isset($system['auth']) && $system['auth'] == true) {
                $log_button = '<a class="btn btn-primary" href="/home/logout">Logout</a>';
            } else {
                $log_button = '<a class="btn btn-primary" href="/home/login">Login</a>';
            }

            $view = str_replace('{{ log_button }}', $log_button, $view);

            return $view;
        }

        http_response_code(404);
        throw new Exception('View cannot be found: ' . $viewPath . '.', 404);
    }

    private function validateVariables(array $variables = []) {
        foreach ($variables as $name => $value) {
            if (in_array($name, $this->reservedVariables)) {
                return false;
            }
        }
        return true;
    }

    private function getDirectory($controller) {
        $parts = explode('\\', $controller);
        return end($parts);
    }

    private function getFile($controller) {
        return str_replace(APP_CONTROLLER_METHOD_SUFFIX, null, $controller);
    }

}