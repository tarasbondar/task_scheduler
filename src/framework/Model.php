<?php

namespace Framework;

abstract class Model {

    protected static function getConnect() {
        $conn = Connect::instance()->getConnection( DB_NAME, DB_HOST, DB_USER, DB_PASS );
        return $conn;
    }

    public function get($id) {
        $query = $this->getConnect()->prepare("SELECT * FROM " . $this->table . " WHERE id = ?");
        $query->execute([$id]);
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        $response = isset($result[0]) ? $result[0] : NULL;
        return $response;
    }

    public function getBy($key, $value) {
        $query = $this->getConnect()->prepare("SELECT * FROM " . $this->table . " WHERE " . $key . " = :value");
        $query->execute(['value' => $value]);
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return ($result ?: NULL);
    }

    public function save($data) {
        if (empty($data)) { return 0; }

        if (isset($data['id'])) {
            $id = $data['id']; unset($data['id']);
        } else {
            $id = 0;
        }

        foreach ($data as $k => $v) {
            if (!in_array($k, $this->fields)) {
                unset($data[$k]);
            }
        }

        if (!empty($data)) {
            $fields = array_keys($data);
            $values = array_values($data);

            if ($id > 0) { //update
                $prep_fields = [];
                for ($i = 0; count($fields) > $i; $i++) {
                    $prep_fields[]= $fields[$i] . ' = :' . $fields[$i];
                }

                $query = $this->getConnect()->prepare("UPDATE " . $this->table . " SET " . implode(', ', $prep_fields) . " WHERE id = :id");
                $query->bindParam(':id', $id);
                for ($i = 0; count($values) > $i; $i++) {
                    $query->bindParam(':'.$fields[$i], $values[$i]);
                }
                $query->execute();
                return $id;
            } else { //create
                $query = $this->getConnect()->prepare("INSERT INTO " . $this->table . " (" . implode(', ', $fields) . ") VALUES (:" . implode(', :', $fields). ")" );
                for ($i = 0; count($values) > $i; $i++) {
                    $query->bindParam(':'.$fields[$i], $values[$i]);
                }
                $query->execute();
                return $this->getLastInsertedId();
            }
        } else {
            return 0;
        }

    }

    public function count() {
        return $this->getConnect()->query("SELECT COUNT(id) AS d FROM " . $this->table)->fetch(2)['d'];
    }

    public function getLastInsertedId() {
        return $this->getConnect()->query("SELECT MAX(id) AS d FROM " . $this->table)->fetch(2)['d'];
    }

}