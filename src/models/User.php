<?php

namespace Model;

use Framework\Model;

class User extends Model {

    protected $table = 'users';

    protected $fields = ['id', 'name', 'email', 'password', 'session'];

    public function auth($username, $password) {
        $query = $this->getConnect()->query("SELECT * FROM " . $this->table . " WHERE name = '" . $username . "' AND password = '" . sha1($password) . "'")->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($query[0])) {
            $session = sha1($query[0]['id'] . time());

            $query = $this->getConnect()->prepare("UPDATE " . $this->table . " SET session = :session WHERE id = " . $query[0]['id']);
            $query->bindParam(':session', $session);
            $query->execute();

            setcookie('user', $session, time() + (86400 * 7), "/");
            return true;
        } else {
            return false;
        }

    }

    public function logout() {
        $query = $this->getConnect()->prepare("UPDATE " . $this->table . " SET session = '' WHERE session = " . $_COOKIE['user']);
        $query->execute();
        setcookie('user', null, -1, "/");
        return true;
    }

}