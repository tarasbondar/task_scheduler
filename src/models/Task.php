<?php

namespace Model;

use Framework\Model;

class Task extends Model {

    protected $table = 'tasks';

    protected $fields = ['id', 'title', 'description', 'username', 'email', 'status', 'updated_by'];

    protected $page_size = 3;

    function __construct() {

    }

    public function getPageSize() {
        return $this->page_size;
    }

    public function getAll($page = 1, $field = 'id', $order = 'ASC') {
        $statement = "SELECT * FROM " . $this->table . " ORDER BY $field $order" . " LIMIT " . $this->page_size . " OFFSET " . (($page - 1) * $this->page_size);
        $query = $this->getConnect()->query($statement)->fetchAll(\PDO::FETCH_ASSOC);
        $results = [];
        foreach ($query as $result) {
            $results[]=$result;
        }
        return $results;
    }

}