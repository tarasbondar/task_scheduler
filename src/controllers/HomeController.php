<?php

namespace Controller;

use Model\Task;
use Model\User;

class Home extends AbstractController
{
    public function __construct() {
        parent::__construct();
    }

    public function indexMethod() { // /page/field/direction
        $taskModel = new Task();
        $page = @$this->args[0] ?: 1;
        $field = @$this->args[1] ?: 'username';
        $dir = @$this->args[2] ?: 'ASC';
        $tasks = $taskModel->getAll($page, $field, $dir);
        $markup = $this->getMarkup($tasks);

        return parent::getView(
            __METHOD__, [ 'page_title' => 'Task Scheduler', 'tasks' => $markup, 'pagination' => $this->getPagination($page, $taskModel->count(), $taskModel->getPageSize()) ], ['auth' => $this->isAuth()]
        );

    }

    public function addMethod() {
        $name = $email = '';
        if ($this->isAuth()) {
            $user = $this->getCurrentUser();
            if (!empty($user)) {
                $name = $user['name'];
                $email = $user['email'];
            }
        }
        return parent::getView(
            __METHOD__, [ 'page_title' => 'Add task', 'username' => $name, 'email' => $email, 'description' => '', 'title' => ''], ['auth' => $this->isAuth()]
        );
    }

    public function editMethod() {
        if ($this->isAuth()) {
            $task_id = $this->args[0];
            if (!empty($task_id)) {
                $task = (new Task())->get($task_id);
                $name = $task['username'];
                $email = $task['email'];
                $title = $task['title'];
                $descr = $task['description'];

                return parent::getView(
                    'Controller\Home::addMethod', ['page_title' => 'Edit task', 'username' => $name, 'email' => $email, 'description' => $descr, 'title' => $title], ['auth' => $this->isAuth()]
                );
            }
        }
        header("Location: /home/add");
    }

    public function loginMethod() {
        return parent::getView(
            __METHOD__, [ 'page_title' => 'Log In', 'header' => 'Log In', 'username' => 'Username' ], ['auth' => $this->isAuth()]
        );
    }

    public function logoutMethod() {
        (new User)->logout();
        header("Location: /home/index");
    }

    public function getPagination($page, $total, $page_size) {
        $markup = '';
        $last_page = ceil($total / $page_size);

        if ($last_page > 1) {

            $field = @$this->args[1] ?: 'username';
            $dir = @$this->args[2] ?: 'ASC';
            $prev_page = $page - 1;
            $next_page = $page + 1;

            if ($page == 1) {
                $markup .= "<li class='page-item disabled'><a class='page-link' href='#'>First</a></li>";
            } else {
                $link = "/home/index/1/{$field}/{$dir}";
                $markup .= "<li class='page-item'><a class='page-link' href='{$link}'>First</a></li>";
            }

            if ($prev_page > 0) {
                $link = "/home/index/{$prev_page}/{$field}/{$dir}";
                $markup .= "<li class='page-item'><a class='page-link' href='{$link}'>" . $prev_page . "</a></li>";
            }

            $link = "/home/index/{$page}/{$field}/{$dir}";
            $markup .= "<li class='page-item disabled'><a class='page-link' href='{$link}'>" . $page . "</a></li>";

            if ($next_page <= $last_page) {
                $link = "/home/index/{$next_page}/{$field}/{$dir}";
                $markup .= "<li class='page-item'><a class='page-link' href='{$link}'>" . $next_page . "</a></li>";
            }

            if ($page == $last_page) {
                $markup .= "<li class='page-item disabled'><a class='page-link' href='#'>Last</a></li>";
            } else {
                $link = "/home/index/{$last_page}/{$field}/{$dir}";
                $markup .= "<li class='page-item'><a class='page-link' href='{$link}'>Last</a></li>";
            }

        }
        
        return $markup;
    }

    public function getMarkup($tasks) {
        $markup = '';
        foreach ($tasks as $t) {
            $status = ($t['status'] == 1 ? 'Completed' : 'In progress');

            $markup .= "<div class='col-md-4 mt-4' id='task-{$t['id']}'><div class='row'><div class='col-md-10'><h4>{$t['title']} </h4></div>";
            if ($this->isAuth()) {
                $markup .= "<div class='col-md-2'><a style='text-decoration: none' href='/home/edit/{$t['id']}'>Edit</a></div>";
            }

            $markup .= "</div><p>Created by: {$t['username']} ({$t['email']}) ";
            if (!empty($t['updated_by'])) {
                $markup .= '| <span>Updated by admin</span>';
            }
            $markup .= "</p><p>{$t['description']}</p><p class='task-status'>Status: <span>{$status}</span></p>";
            if ($this->isAuth() && $t['status'] == 0) {
                $markup .= "<div class='form-check'><input class='form-check-input' type='checkbox' value='{$t['id']}' id='check{$t['id']}'><label class='form-check-label' for='check{$t['id']}'> Set as completed </label></div>";
            }
            $markup .= '</div>';
        }

        return $markup;
    }

    public function strip_tags($input) {
        return str_replace(['<script','<style','<iframe','<link'], '', $input);
    }

}