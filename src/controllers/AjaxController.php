<?php

namespace Controller;

use Model\Task;

class Ajax extends AbstractController {

    public function __construct() {
        if ( !isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            throw new \Exception('No');
        }
        parent::__construct();
    }

    public function checktaskMethod() {
        if ($this->isAuth()) {
            $response = (new Task())->save(['id' => $_POST['id'], 'status' => 1]);
            return json_encode($response);
        } else {
            return 'No';
        }

    }



}