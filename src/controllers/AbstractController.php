<?php

namespace Controller;

use Framework\Template;
use Model\User;

abstract class AbstractController {

    protected $args = [];

    public function __construct() {
        $this->args = $_GET['args'];
    }

    protected function getView($method, array $variables = [], array $system = []) {
        return (new Template())->getView($method, $variables, $system);
    }

    protected function isAuth() {
        if (isset($_COOKIE['user'])) {
            $user = (new User())->getBy('session', $_COOKIE['user']);
            if (!empty($user)) {
                return true;
            }
        }
        return false;
    }

    protected function getCurrentUser() {
        if (isset($_COOKIE['user'])) {
            $user = (new User())->getBy('session', $_COOKIE['user']);
            if (!empty($user)) {
                return $user;
            }
        }
        return NULL;
    }


}