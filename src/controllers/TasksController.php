<?php

namespace Controller;

use Model\Task;
use Model\User;

class Tasks extends AbstractController {

    public function insertMethod() {
        $uri = explode('/', $_SERVER['HTTP_REFERER']);
        $id = end($uri);
        $email = $_REQUEST['email'];
        $descr = strip_tags($_REQUEST['descr']);
        $title = strip_tags($_REQUEST['title']);
        $username = strip_tags($_REQUEST['username']);
        $data = ['id' => $id, 'title' => $title, 'description' => $descr,  'username' => $username, 'email' => $email];
        if ($this->isAuth()) {
            $user = $this->getCurrentUser();
            if ($email != $user['email']) {
                $data['updated_by'] = $user['id'];
            }
        }
        $response = (new Task())->save($data);
        header("Location: /");
    }

}