<?php

namespace Controller;

use Model\User as UserModel;

class User extends AbstractController {

    public function authMethod() {
        $username = $_REQUEST['username'];
        $password = $_REQUEST['password'];
        $response = (new UserModel())->auth($username, $password);
        if ($response) {
            header("Location: /home/index");
        } else {
            header("Location: /home/login");
        }
    }

}
