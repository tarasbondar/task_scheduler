<html>
<head>
    <title>{{ page_title }}</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
    <script type="text/javascript" src="/assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/popper.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.js"></script>
</head>
<body>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal"><a style='text-decoration: none' href="/">Task Scheduler</a></h5>

        <div class="btn-group">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort</button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="/home/index/1/username/asc">Username ASC</a>
                <a class="dropdown-item" href="/home/index/1/username/desc">Username DESC</a>
                <a class="dropdown-item" href="/home/index/1/email/asc">Email ASC</a>
                <a class="dropdown-item" href="/home/index/1/email/desc">Email DESC</a>
                <a class="dropdown-item" href="/home/index/1/status/asc">Status ASC</a>
                <a class="dropdown-item" href="/home/index/1/status/desc">Status DESC</a>
            </div>
        </div>

        <a class="btn btn-outline-primary m-2" href="/home/add">Add task</a>

        {{ log_button }}

    </div>

    <div class="container">
        {{ body }}
    </div>
</body>
</html>