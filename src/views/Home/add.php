<form id="add-task" action="/tasks/insert" method="POST">
    <div class="form-group">
        <label for="name-input">Your Name</label>
        <input type="text" class="form-control" id="name-input" name="username" placeholder="Enter username" value="{{ username }}" required>
    </div>
    <div class="form-group">
        <label for="email-input">Your Email</label>
        <input type="email" class="form-control" id="email-input" name="email" placeholder="Enter email" value ="{{ email }}" required>
    </div>
    <div class="form-group">
        <label for="title-input">Task title</label>
        <input type="text" class="form-control" id="title-input" name="title" placeholder="Enter task title" value ="{{ title }}" required>
    </div>
    <div class="form-group">
        <label for="descr-input">Description</label>
        <textarea class="form-control" id="descr-input" name="descr" rows="3">{{ description }}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>