<form id="login-form" method="POST" action="/user/auth">
    <div class="form-group row">
        <label for="username-input" class="col-sm-2 col-form-label">Username</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="username-input" name="username" placeholder="{{ username }}" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="password-input" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="password-input" name='password' placeholder="Password" required>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Log In</button>
</form>