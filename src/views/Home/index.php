<table>

    <div class="container">
        <div class="row my-5">
            {{ tasks }}
        </div>
        <div class="row">
            <nav>
                <ul class="pagination">
                    {{ pagination }}
                </ul>
            </nav>
        </div>

    </div>

</table>

<script type="text/javascript">

    $(document).ready(function(){
        $('.form-check-input').click(function(){
            if (confirm('Are you sure?')) {
                let div = $(this).parent().parent();
                $.ajax({
                    method: 'POST',
                    data: {id: div.attr('id').split('-').pop()},
                    url: '/ajax/checktask',
                    success: function(r) {
                        if (r === 'true') {
                            div.find('.form-check').html('');
                            div.find('p.task-status').find('span').html('Completed');
                        } else {
                            $(this).prop('checked', false);
                        }
                    }
                })
            } else {
                $(this).prop('checked', false);
            }

        });
    });

</script>