<?php
define('APP_NAME', 'Task Scheduler');
define('APP_DOMAIN', 'localhost');
define('APP_ROOT', __DIR__.'/..');

define('APP_CONTROLLER_NAMESPACE', 'Controller\\');
define('APP_DEFAULT_CONTROLLER', 'Home');
define('APP_DEFAULT_CONTROLLER_METHOD', 'index');
define('APP_CONTROLLER_METHOD_SUFFIX', 'Method');

define('DB_HOST', 'localhost');
define('DB_NAME', 'task_scheduler');
define('DB_USER', 'root');
define('DB_PASS', '');